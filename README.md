# YAK Rover Z1

## WIP: Setup

Assuming an RPi 4.

* Prepare the RPi to free the serial port for the servo controller. [See below](#rpi_settings)
* Mount a USB drive to host all code and data. All scripts assume `/media/pi/z1-drive` mount point for now.
* Add drive to `/etc/fstab` for automatic mount (expected for now). [See below](#add-to-fstab).
* Use `raspi-config` or alike to activate SSH.
* Install dependencies [See below](#dependencies).
* Configure CRON to ensure assumptions: `@reboot sleep 30 && bash -x /media/pi/z1-drive/yak/scripts/ensure_access.sh`
* Issue manually once: `ssh vps_user@ip_to_vps` so as to accept the SSH requirement on the fingerprint (then automation will work).

### RPi Settings

The RPi 4 with Raspbian (32 or 64 bits) uses the primary serial ports for BlueTooth and a serial console. We need to deactivate all for the HiWonder servo controller to work:

* Add `dtoverlay=disable-bt` at the end of `/boot/config.txt`. The change requires a reboot afterward.
* The above MAY BE enough. If not (unclear at this point, need more trials), please also apply the following.

Methods differ on different RPI versions, causing confusing in the documentations. If the above was not enough, it worked when adding some or all of the following:

* Disable the serial console with `raspi-config`, excellent detail [in the documentation](https://www.raspberrypi.com/documentation/computers/configuration.html#disabling-the-linux-serial-console), essentially:
  1. Start raspi-config: sudo raspi-config.
  2. Select option 3 - Interface Options.
  3. Select option P6 - Serial Port.
  4. At the prompt Would you like a login shell to be accessible over serial? answer 'No'
  5. At the prompt Would you like the serial port hardware to be enabled? answer 'Yes'
  6. Exit raspi-config and reboot the Pi for changes to take effect.
* Disable `getty` with `sudo systemctl stop getty.target`, then `sudo systemctl disable getty.target`, and finally `sudo systemctl mask getty.target` (there should be simpler way, but it did not work once without the masking).

### Add to fstab

After mounting the USB drive, you can use `blkid` to get the disk UUID. Then, you can add to `/etc/fstab`:

    UUID=829b0ff3-3c65-4f81-a43d-d012b27f0d03  /media/pi/z1-drive  ext4  rw,user,auto  0  0

Where the UUID should be replaced with the one returned by your tool (here just an example).

### Dependencies

```
sudo apt-get install screen git autossh pigpio python3-pip
```
