import pytest

from bos.sensor import Sensor


def test_sensor_read():
    '''
    A sensor without implementation should raise NotImplemented on #read
    '''
    with pytest.raises(NotImplementedError):
        Sensor().read()


def test_sensor_stream():
    '''
    A sensor without implementation should raise NotImplemented on #read
    '''
    with pytest.raises(NotImplementedError):
        Sensor().stream()
