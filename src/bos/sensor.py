from typing import Dict

from bos.data import Reading, Stream


class Sensor(object):
    def read(self, options: Dict = {}) -> Reading:
        raise NotImplementedError

    def stream(self, options: Dict = {}) -> Stream:
        raise NotImplementedError
