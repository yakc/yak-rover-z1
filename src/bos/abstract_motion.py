import asyncio
from asyncio import Future
from typing import Dict


class Motion(object):
    """
    Using asyncio.run should be safer, but as of Python 3.9 and 3.10
    it is unclear what the semantic is (and is changing).
    """

    def __init__(self):
        self.loop = asyncio.get_event_loop()

    def __del__(self):
        if sys.version_info.major >= 3 and sys.version_info.minor >= 9:
            self.loop.run_until_complete(self.loop.shutdown_default_executor())
        self.loop.run_until_complete(self.loop.shutdown_asyncgens())
        self.loop.close()

    def move(self, axis: Dict[str, float], options: Dict = {}) -> bool:
        task = self.loop.create_task(self.move_async(axis, options))
        result = self.loop.run_until_complete(task)
        return result.done()

    async def move_async(self, axis: Dict[str, float], options: Dict = {}) -> Future:
        # print("I ran")
        # f = self.loop.create_future()
        # f.set_result(True)
        # return f
        raise NotImplementedError
