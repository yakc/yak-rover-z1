from setuptools import setup, find_namespace_packages

setup(
    author='Eric Platon',
    author_email='zaraki@gmx.com',
    description='Exploration library on rover software modules.',
    install_requires=[
        'numpy',
    ],
    keywords='rover bot robot control introspection safety',
    name='bos',
    packages=find_namespace_packages(
        where='src',
        exclude=[]
    ),
    package_data={
        '': [ '*.hdf5', '*.bin' ],
    },
    package_dir={'': 'src'},
    url='https://gitlab.com/yakc/yak-rover-z1',
    version='0.1',
)
