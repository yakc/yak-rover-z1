#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
import hcsr04
import time
import threading
import hexapod
import Serial_Servo_Running as SSR

distance_ok = False
distance = 0.0

GPIO_TRIG = 12   # 超声波trig引脚对应的IO号
GPIO_ECHO = 16   # 超声波echo引脚对应的IO号

sonar = hcsr04.Measurement(GPIO_TRIG, GPIO_ECHO)

def move():
    global distance
    global distance_ok
    while True:
        print(distance)
        if distance_ok:
            if distance > 40.0 or distance == 0:
                distance_ok = False
                SSR.run_ActionGroup("41", 1)
            else:
                # print("T")
                hexapod.turn(10, 200)
                hexapod.turn(10, 200)
                hexapod.turn(10, 200)
                # hexapod.turn(11.25, 100)
                distance_ok = False

        else:
            time.sleep(0.01)


th1 = threading.Thread(target=move)
th1.setDaemon(True)     # 设置为后台线程，这里默认是False，设置为True之后则主线程不用等待子线程
th1.start()

hexapod.hexapod_init()
hexapod.camera_pos_init()
if __name__ == '__main__':
    print ("超声波避障")
    while True:
        if distance_ok is False:
            try:
                distance = sonar.distance_metric(sonar.raw_distance(2, 0.08))
                distance_ok = True
            except:
                pass  
        else:
            time.sleep(0.01)
