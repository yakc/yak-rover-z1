#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import math
import SerialServoCmd as Servoctrl
import time
import PWMServo

def camera_pos_init():
    PWMServo.setServo(1, 1500, 100)
    time.sleep(0.1)
    PWMServo.setServo(2, 1500, 100)
    time.sleep(0.1)
if __name__ == '__main__':
	while True:
	    # 参数：参数1：舵机接口编号; 参数2：位置; 参数3：运行时间
	    PWMServo.setServo(2, 1500, 500)  # 2号舵机转到1500位置，用时500ms
	    time.sleep(0.5)  # 延时时间和运行时间相同
	    
	    PWMServo.setServo(2, 1800, 500)  #舵机的转动范围0-180度，对应的脉宽为500-2500,即参数2的范围为500-2500
	    time.sleep(0.5)
	    
	    PWMServo.setServo(2, 1500, 200)
	    time.sleep(0.2)
	    
	    PWMServo.setServo(2, 1800, 500)  
	    PWMServo.setServo(1, 1800, 500)
	    time.sleep(0.5)
	    
	    PWMServo.setServo(2, 1500, 500)  
	    PWMServo.setServo(1, 1500, 500)
	    time.sleep(0.5)    

