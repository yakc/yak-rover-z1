#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
import cv2
import numpy as np
import time
import threading
import pid
import math
import PWMServo
from cv_ImgAddText import *
import Serial_Servo_Running as SSR
from lab_conf import color_range
import hexapod

print('''
**********************************************************
*****颜色跟踪:识别对应颜色的小球,让六足跟随小球移动*****
**********************************************************
----------------------------------------------------------
Official website:http://www.lobot-robot.com/pc/index/index
Online mall:https://lobot-zone.taobao.com/
----------------------------------------------------------
Version: --V3.0  2019/08/10
----------------------------------------------------------
''')

update_ok = False
img_w = 480
img_h = 360
ball_x = 0
angle_factor = 0.167
orgFrame = None
radius = 0
Get_Angle = False
angle = 0.0
Running = True

stream = "http://127.0.0.1:8080/?action=stream?dummy=param.mjpg"
cap = cv2.VideoCapture(stream)

def get_image():
    global orgFrame
    global Running
    while True:
        if Running:
            if cap.isOpened():
                ret, orgFrame = cap.read()
            else:
                time.sleep(0.01)
        else:
            time.sleep(0.01)

th1 = threading.Thread(target=get_image)
th1.setDaemon(True)
th1.start()

def follow():
    global ball_x
    global img_w
    global Get_Angle
    global radius
    while True:
        if Get_Angle:
            if ball_x < img_w/2 - 80 :
                hexapod.turn(-3, 100)
                Get_Angle = False
            elif ball_x > img_w/2 + 80:
                hexapod.turn(3, 100)
                Get_Angle = False
            else:
                if 5 < radius <= 35:
                    SSR.run_ActionGroup("41", 1)
                else:
                    time.sleep(0.01)
        else:
            time.sleep(0.01)

th2 = threading.Thread(target=follow)
th2.setDaemon(True)     # 设置为后台线程，这里默认是False，设置为True之后则主线程不用等待子线程
th2.start()

hexapod.camera_pos_init()
hexapod.hexapod_init()
    
while True:
    get_color = False
    if orgFrame is not None:
        t1 = cv2.getTickCount()
        orgframe = cv2.resize(orgFrame, (img_w, img_h))
        res = orgframe.copy()
        frame = orgframe.copy()
        lab = cv2.cvtColor(res, cv2.COLOR_BGR2LAB)
        mask = cv2.inRange(lab, color_range['red'][0], color_range['red'][1])
        opened = cv2.morphologyEx(mask, cv2.MORPH_OPEN, np.ones((3,3),np.uint8))#开运算
        closed = cv2.morphologyEx(opened, cv2.MORPH_CLOSE, np.ones((3,3),np.uint8))#闭运算
        # 查找轮廓
        cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            # 求出最小外接圆  原点坐标x, y  和半径
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            ball_x = int(x)

            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
            cv2.circle(frame, (int(x), int(y)), 5, (0, 255, 255), -1)

            if Get_Angle is False:
                angle = int((ball_x - img_w/2) * angle_factor)
                Get_Angle = True
            else:
                Get_Angle = False

        orgframe = cv2ImgAddText(frame, "颜色跟踪", 10, 10, textColor=(0, 0, 0), textSize=20)
        cv2.line(frame, (int(img_w / 2) - 20, int(img_h / 2)), (int(img_w / 2) + 20, int(img_h / 2)), (255, 255, 0), 1)
        cv2.line(frame, (int(img_w / 2), int(img_h / 2) - 20), (int(img_w / 2), int(img_h / 2) + 20), (255, 255, 0), 1)
        t2 = cv2.getTickCount()
        time_r = (t2 - t1) / cv2.getTickFrequency()
        fps = 1.0 / time_r
        cv2.putText(frame, "FPS:" + str(int(fps)),(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)  # (0, 0, 255)BGR
        cv2.imshow('cv_ball_frame', frame)
        cv2.waitKey(1)
    else:
        time.sleep(0.01)