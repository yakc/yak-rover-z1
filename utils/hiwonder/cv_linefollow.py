#!/usr/bin/python3
# encoding: utf-8
# 小球颜色识别后执行动作组

import cv2
import numpy as np
import time
import threading
import hexapod
import PWMServo
import math
from cv_ImgAddText import *
import Serial_Servo_Running as SSR
from lab_conf import color_range

print('''
**********************************************************
*********智能巡迹:识别出黑线,让六足沿着黑线行走***********
**********************************************************
----------------------------------------------------------
Official website:http://www.lobot-robot.com/pc/index/index
Online mall:https://lobot-zone.taobao.com/
----------------------------------------------------------
Version: --V3.0  2019/08/10
----------------------------------------------------------
''')

last_turn = None
orgFrame = None
Running = True
get_image_ok = False
cv_ok = False
angle_factor = 0.125
line_out = False

stream = "http://127.0.0.1:8080/?action=stream?dummy=param.mjpg"
cap = cv2.VideoCapture(stream)

# 机器人应该转的角度
deflection_angle = 0

def get_image():
    global orgFrame
    global Running
    while True:
        if Running:
            if cap.isOpened():
                ret, orgFrame = cap.read()
            else:
                time.sleep(0.01)
        else:
            time.sleep(0.01)

th1 = threading.Thread(target=get_image)
th1.setDaemon(True)
th1.start()


def get_x(img):
    '''
    范围区域图像内色块的中心坐标X
    :param img:
    :return:
    '''
    x = 0
    # 高斯模糊
    gs_frame = cv2.GaussianBlur(img, (5, 5), 0)
    # 转换颜色空间
    lab = cv2.cvtColor(gs_frame, cv2.COLOR_BGR2LAB)
    # 查找颜色
    mask = cv2.inRange(lab, color_range['black'][0], color_range['black'][1])
    opened = cv2.morphologyEx(mask, cv2.MORPH_OPEN, np.ones((3,3),np.uint8))#开运算
    closed = cv2.morphologyEx(opened, cv2.MORPH_CLOSE, np.ones((3,3),np.uint8))#闭运算
    # 查找轮廓
    cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(cnts):
        c = max(cnts, key=cv2.contourArea)  # 找出最大的区域
        area = cv2.contourArea(c)
        # 获取最小外接矩形
        rect = cv2.minAreaRect(c)
        if area >= 500:
            xy = rect[0]
            xy = int(xy[0]), int(xy[1])
            cv2.circle(img, (xy[0], xy[1]), 3, (0, 255, 0), -1)
            x = xy[0]
            box = cv2.boxPoints(rect)
            # 数据类型转换
            box = np.int0(box)
            # 绘制轮廓
            cv2.drawContours(img, [box], 0, (0, 255, 255), 1)
    return x


def line():
    global cv_ok
    global deflection_angle
    global line_count, line_flag
    global last_turn
    global line_out
    while True:
        if cv_ok:
            if -20 <= deflection_angle <= 20:
                SSR.run_ActionGroup('41', 1)
                #print("ST")
            else:
                hexapod.turn(deflection_angle / 15, 150)
                time.sleep(0.15)
            cv_ok = False
        else:
            if line_out:
                if last_turn == 'R':
                    #print ("last_turn")
                    hexapod.turn(5, 150)
                elif last_turn == 'L':
                    #print ("last_turn")
                    hexapod.turn(-5, 150)
                line_out = False
            else:
                time.sleep(0.05)


th2 = threading.Thread(target=line)
th2.setDaemon(True)     # 设置为后台线程，这里默认是False，设置为True之后则主线程不用等待子线程
th2.start()

def camera_pos_init():
    PWMServo.setServo(2, 1500, 200)
    time.sleep(0.2)
    PWMServo.setServo(1, 1000, 200)
    time.sleep(0.2)

SSR.run_ActionGroup('25', 1)
camera_pos_init()    
x_list = []
line_center = 0.0    
while True:
    if orgFrame is not None:
        t1 = cv2.getTickCount()
        f = orgFrame.copy()
        # 获取总图像的大小
        img_h, img_w = f.shape[:2]
        up_frame = f[0:65, 0:480]
        center_frame = f[145:210, 0:480]
        down_frame = f[290:355, 0:480]

        up_x = get_x(up_frame)
        center_x = get_x(center_frame)
        down_x = get_x(down_frame)

        if down_x != 0:
            line_center = down_x
            # print('c_x', deflection_angle)
            if line_center >= 360:
                last_turn = 'R'
            elif line_center <= 120:
                last_turn = 'L'

            d_line = line_center - img_w / 2
            deflection_angle = d_line * angle_factor
            # print("offset", deflection_angle)
            cv_ok = True
        elif center_x != 0:
            line_center = center_x
            # print('d_x', deflection_angle)
            if line_center >= 360:
                last_turn = 'R'
            elif line_center <= 120:
                last_turn = 'L'

            d_line = line_center - img_w / 2
            deflection_angle = d_line * angle_factor
            # print("offset", deflection_angle)
            cv_ok = True
        elif up_x != 0 and down_x != 0:
            line_center = (up_x + down_x) / 2
            # print('ud_x', deflection_angle)
            d_line = line_center - img_w / 2
            deflection_angle = d_line * angle_factor
            # print("offset", deflection_angle)
            cv_ok = True
        elif up_x != 0:
            line_center = up_x
            if line_center >= 360:
                last_turn = 'R'
            elif line_center <= 120:
                last_turn = 'L'
            # print('u_x', deflection_angle)
            d_line = line_center - img_w / 2
            deflection_angle = d_line * angle_factor
            # print("offset", deflection_angle)
            cv_ok = True
        elif up_x == 0 and down_x == 0 and center_x == 0:
            line_out = True

        # 画屏幕中心十字
        cv2.line(f, (int(img_w / 2) - 20, int(img_h / 2)), (int(img_w / 2) + 20, int(img_h / 2)), (255, 255, 0), 1)
        cv2.line(f, (int(img_w / 2), int(img_h / 2) - 20), (int(img_w / 2), int(img_h / 2) + 20), (255, 255, 0), 1)
        orgframe = cv2ImgAddText(f, "智能巡迹", 10, 10, textColor=(0, 0, 0), textSize=20)
        cv2.namedWindow("cv_ball_frame", cv2.WINDOW_AUTOSIZE)
        t2 = cv2.getTickCount()
        time_r = (t2 - t1) / cv2.getTickFrequency()
        fps = 1.0 / time_r
        cv2.putText(f, "FPS:" + str(int(fps)),
                    (10, f.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)  # (0, 0, 255)BGR
        cv2.imshow('cv_ball_frame', f)
        cv2.waitKey(1)
    else:
        time.sleep(0.01)

