#!/usr/bin/env bash

apt-get -y update
apt-get -y install \
	git \
	pigpiod \
	python3-pip \
	vim

cp pigpiod.service /etc/systemd/system

systemctl enable pigpiod.service
systemctl start pigpiod.service
