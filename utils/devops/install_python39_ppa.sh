#!/usr/bin/env bash

set -e

apt-get install software-properties-common
add-apt-repository ppa:deadsnakes/ppa
apt-get update

apt-get install python3.9

update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1

update-alternatives --config python3
